/*jshint esversion: 6 */
const express = require("express");
const fs = require("fs");
const sqlite = require("sql.js");
const filebuffer = fs.readFileSync("db/usda-nnd.sqlite3");
const db = new sqlite.Database(filebuffer);
const app = express();
app.set("port", process.env.PORT || 3001);
const amazon = require('amazon-product-api');
const client = amazon.createClient({
  awsId: "AKIAJJYS22CYVBWS6OAA",
  awsSecret: "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",
  awsTag: "1028f1-20"
});
// Express only serves static assets in production
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
}


class Model {
  constructor(){

  }
  deleteTables(){
    const query = `
      DROP TABLE IF EXISTS brands;
      DROP TABLE IF EXISTS pantsSizes; 
      DROP TABLE IF EXISTS jacketSizes;
      DROP TABLE IF EXISTS pants;
      DROP TABLE IF EXISTS jacket;
      DROP TABLE IF EXISTS matches;
    `
    this._runQueryWithCommit(query);
  } 
  emptyTables(){
    if (process.env.NODE_ENV === "production") {
      return;
    }
    const query = `
        DELETE FROM brands;
        DELETE FROM pantsSizes;
        DELETE FROM jacketSizes;
        DELETE FROM pants;
        DELETE FROM jacket;
      `
    this._runQueryWithCommit(query);
  } 
  createTables(){
    const query = `
          CREATE TABLE IF NOT EXISTS brands (
            brandid integer primary key autoincrement, 
            name varchar(30)
          );
          CREATE TABLE IF NOT EXISTS pantsSizes (
            sizeid integer primary key autoincrement, 
            name varchar(30)
          );
          CREATE TABLE IF NOT EXISTS jacketSizes (
            sizeid integer primary key autoincrement, 
            name varchar(30)
          );
          CREATE TABLE IF NOT EXISTS pants (
            pantsid integer primary key autoincrement, 
            sizeid int,
            brandid int,
            ASIN varchar(10),
            title varchar(100),
            color varchar(100),
            detailPageURL varchar(255),
            mediumImage varchar(100),
            largeImage varchar(100),
            LowestNewPrice int,
            LowestNewPriceFormatted varchar(10),
            moreOffersUrl varchar(100)
          );
          CREATE TABLE IF NOT EXISTS jacket (
            jacketid integer primary key autoincrement, 
            sizeid int,
            brandid int,
            ASIN varchar(10),
            title varchar(100),
            color varchar(100),
            detailPageURL varchar(255),
            mediumImage varchar(100),
            largeImage varchar(100),
            LowestNewPrice int,
            LowestNewPriceFormatted varchar(10),
            moreOffersUrl varchar(100)
          );
          CREATE TABLE IF NOT EXISTS matches (
            matchid integer primary key autoincrement, 
            jacketid,
            pantsid
          );
    `
    this._runQueryWithCommit(query);
  }
  insertClothing(opts){
    let query = "INSERT INTO "+opts.db_table+"(sizeid, brandid, ASIN,title, color, detailPageURL, mediumImage, largeImage, LowestNewPrice,LowestNewPriceFormatted,moreOffersUrl) ";
        query += "SELECT " +
              "(SELECT sizeid from "+opts.sizes_table+" s WHERE s.name = '"+opts.clothingSize+"' LIMIT 1)," +
              "(SELECT brandid from brands b WHERE b.name = '"+opts.brand+"' LIMIT 1)," +
              "'" + opts.ASIN + "'," +
              "'" + opts.title + "'," +
              "'" + opts.color + "'," +
              "'" + opts.detailPageURL+ "'," +
              "'" + opts.mediumImage+ "'," +
              "'" + opts.largeImage+ "'," +
              "'" + opts.LowestNewPrice+ "'," +
              "'" + opts.LowestNewPriceFormatted+ "'," +
              "'" + opts.moreOffersUrl + "'" +
            "WHERE NOT EXISTS(SELECT * FROM "+opts.db_table+" WHERE ASIN = '"+opts.ASIN+"');"
    this._runQueryWithCommit(query);
  }
  insertBrand(brand){
    let query ="INSERT INTO brands(name) ";
        query += "SELECT '"+brand+"' WHERE NOT EXISTS(SELECT * FROM brands WHERE name = '"+brand+"');"; 
    this._runQueryWithCommit(query);
  } 
  getBrands(jSizeID, pSizeID){
    let query = "SELECT DISTINCT b.* from brands b "+
              "INNER JOIN pants p ON p.brandid = b.brandid "+
              "INNER JOIN jacket j ON j.brandid = p.brandid " +
              "INNER JOIN matches m ON m.jacketid = j.jacketid AND m.pantsid = p.pantsid " +
              "WHERE 1=1 ";
    if(jSizeID){
      queryn += "AND j.sizeid = " + jSizeID + " ";
    }
    if(pSizeID){
      queryn += "AND p.sizeid = " + pSizeID + " ";
    }
    query += "ORDER BY b.name;";
    return this._runQuery(query);
  }
  insertSize(clothingSize, db_table){
    let query = "INSERT INTO "+db_table+"(name) " +
                 "SELECT '"+clothingSize+"' WHERE NOT EXISTS(SELECT * FROM "+db_table+" WHERE name = '"+clothingSize+"');";
    this._runQueryWithCommit(query);
  } 
  getPantsSizes(brandid, jSizeID){
    let query = "SELECT DISTINCT ps.* from pantsSizes ps "+
              "INNER JOIN pants p ON p.sizeid = ps.sizeid "+
              "INNER JOIN matches m ON m.pantsid = p.pantsid " +
              "INNER JOIN jacket j ON j.jacketid = m.jacketid " +
              "WHERE 1=1 ";
    if(brandid){
      query += "AND p.brandid = " + brandid + " ";
    }  
    if(jSizeID){
      query += "AND j.sizeid = " + jSizeID + " ";
    }
    query += "ORDER BY ps.name;";
    console.log(query);
    return this._runQuery(query);
  }
  getJacketSizes(brandid, pSizeID){
    let query = "SELECT DISTINCT js.* from jacketSizes js "+
              "INNER JOIN jacket j ON j.sizeid = ps.sizeid "+
              "INNER JOIN matches m ON m.jacketid = p.jacketid " +
              "INNER JOIN pants p ON p.pantsid = m.pantsid " +
              "WHERE 1=1 ";
    if(brandid){
      query += "AND j.brandid = " + brandid + " ";
    } 
    if(pSizeID){
      query += "AND p.sizeid = " + pSizeID + " ";
    }  
    return this._runQuery(query);
  }  
  generateMatches(){
    this._runQueryWithCommit(
        `
          INSERT INTO matches (jacketid, pantsid)
          SELECT j.jacketid,
                 p.pantsid
          FROM jacket j
          INNER JOIN pants p ON j.brandid = p.brandid AND j.color = p.color;

        `
    )
  }
  getMatches(psize, jsize, brandid){
    let query = "SELECT m.jacketid,"+ 
                    "m.pantsid," +
                    "p.title AS ptitle," + 
                    "p.color AS pcolor," + 
                    "p.detailPageURL AS pdetailPageURL," + 
                    "p.mediumImage AS pmediumImage," + 
                    "p.largeImage AS plargeImage," + 
                    "p.LowestNewPrice AS pLowestNewPrice," + 
                    "p.LowestNewPriceFormatted AS pLowestNewPriceFormatted," + 
                    "p.moreOffersUrl AS pmoreOffersUrl," +
                    "j.title AS jtitle," +
                    "j.color AS jcolor," +
                    "j.detailPageURL AS jdetailPageURL," +
                    "j.mediumImage AS jmediumImage," +
                    "j.largeImage AS jlargeImage," +
                    "j.LowestNewPrice AS jLowestNewPrice," +
                    "j.LowestNewPriceFormatted AS jLowestNewPriceFormatted," +
                    "j.moreOffersUrl AS jmoreOffersUrl " +
              "FROM matches m " +
              "INNER JOIN jacket j ON j.jacketid = m.jacketid " +
              "INNER JOIN pants p ON p.pantsid = m.pantsid " +
              "WHERE 1 = 1 "; //
    if(psize){
      query += "AND p.sizeid = '" + psize +"' ";
    }
    if(jsize){
      query += "AND j.sizeid = '" + jsize +"' ";
    }
    if(brandid){
      query += "AND j.brandid = " + brandid + " "+
               "AND p.brandid = " + brandid + " ";
    }
    query += ' LIMIT 100;'
    return this._runQuery(query);
  }  
  _runQuery(queryStr){
    return this._queryToArray(db.exec(queryStr));
  }
  _runQueryWithCommit(queryStr){
    db.exec("BEGIN TRANSACTION;" + queryStr + "COMMIT;" );
    return;
  }
  _queryToArray(query){
    const array =[];
    if(query[0]){
      query[0].values.map(function(value, index){
        array.push({});
        query[0].columns.map(function(column, columnIndex){
          array[index][column] = value[columnIndex];
        });
      });
    }
    return array;
  }
}
const model = new Model();

function fetchData(type, page, cb){
  client.itemSearch({
    keywords: 'separate+'+type,
    itemPage: page,
    searchIndex: 'FashionMen',
    responseGroup: 'Images,ItemAttributes,Offers,Variations,VariationMatrix,VariationOffers,VariationSummary'
  }, function(err, results, response) {
    if(err) {
      console.log(JSON.stringify(err));
    }
    if(cb){
      if(response && response[0] && response[0].TotalPages && response[0].TotalPages[0]){
        cb(results,response[0].TotalPages[0]);
      }else{
        cb();
      }
    }
  });
}
String.prototype.escape = function(){ // escapes apostrophies in sql
  return this.replace(new RegExp("'", 'g'), "''");
}
function insertClothing(Item, db_table, sizes_table){
  let mediumImage = '';
  if(Item.MediumImage && Item.MediumImage[0] && Item.MediumImage[0].URL && Item.MediumImage[0].URL[0]){
    mediumImage = Item.MediumImage[0].URL[0].escape();
  }  
  let largeImage = '';
  if(Item.LargeImage && Item.LargeImage[0] && Item.LargeImage[0].URL && Item.LargeImage[0].URL[0]){
    largeImage = Item.LargeImage[0].URL[0].escape();
  }
  model.insertClothing({
    brand                   : Item.ItemAttributes[0].Brand[0].trim().escape(),
    clothingSize            : Item.ItemAttributes[0].Size[0].trim().toLowerCase().escape(),
    ASIN                    : Item.ASIN[0].escape(),
    title                   : Item.ItemAttributes[0].Title[0].trim().escape(),
    color                   : Item.ItemAttributes[0].Color[0].escape(),
    detailPageURL           : Item.DetailPageURL[0].escape(),
    mediumImage             : mediumImage,
    largeImage              : largeImage,
    LowestNewPrice          : Item.OfferSummary[0].LowestNewPrice[0].Amount[0].escape(),
    LowestNewPriceFormatted : Item.OfferSummary[0].LowestNewPrice[0].FormattedPrice[0].escape(),
    moreOffersUrl           : Item.Offers[0].MoreOffersUrl[0].escape(),
    db_table                : db_table,
    sizes_table             : sizes_table
  })
}
function getProducts(type, cb){
  const maxPages = 10;
  var totalPages = maxPages;
  var page = 1;
  const interval = setInterval(() =>{
    // we don't know the total pages until the first time the API is called.
    fetchData(type, page, (results, numPages) =>{
      console.log('page: ' + page);
      if(numPages){
        totalPages = numPages;
      }
      if(results){
        results.map((Item, index) => {
          // Add the brand
          let brand = Item.ItemAttributes[0].Brand[0].trim().escape();
          let clothingSize = Item.ItemAttributes[0].Size[0].trim().toLowerCase().escape();
          // insert brand
          model.insertBrand(brand) 
          if(type == 'pants'){
            // Add the pants sizes
            model.insertSize(clothingSize, 'pantsSizes')
            // Add the pants
            insertClothing(Item, 'pants', 'pantsSizes');
          }
          if(type == 'jackets'){
            // add the jacket sizes
            model.insertSize(clothingSize, 'jacketSizes')
            insertClothing(Item, 'jacket', 'jacketSizes');
          }
        })
      }
      page++;
    })
    // Amazon API has a maximum of 10 pages
    if(totalPages > maxPages){
      totalPages = maxPages;
    }
    if(totalPages == page){
      clearInterval(interval);
      if(cb){
        cb();
      }  
    } 
  }, 2000);
}  
function init(){
  model.deleteTables();
  model.createTables();
  getProducts('pants', function(){
    getProducts('jackets', function(){
      model.generateMatches();
    })
  });
}  
init();



app.get("/api/brands", (req, res) => {
  const jSizeID = req.query.jsize || 0;
  const pSizeID = req.query.psize || 0;
  res.json(model.getBrands(jSizeID, pSizeID));
  return;
});

app.get("/api/pants-sizes", (req, res) => {
  const jSizeID = req.query.jsize || 0;
  const brandid = req.query.brandid ||0;
  res.json(model.getPantsSizes(brandid, jSizeID));
  return;
});

app.get("/api/jacket-sizes", (req, res) => {
  const pSizeID = req.query.jsize || 0;
  const brandid = req.query.brandid ||0;
  res.json(model.getJacketSizes(brandid, pSizeID));
  return;
});

app.get("/api/matches", (req, res) => {
  const psize = req.query.psize; // pants size
  const jsize = req.query.jsize; // jacket size
  const brandid = req.query.brand;
  res.json(getMatches(psize, jsize, brandid));
  return;
});


app.listen(app.get("port"), () => {
  console.log(`Find the server at: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});
