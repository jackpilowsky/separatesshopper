import React, { Component } from 'react';
import Client from "./../Client.js";
import SelectOption from "./SelectOption.jsx";

class SearchBar extends Component{
  constructor(props){
    super(props);
    this.state = {
      brands: [],
      pantsSizes: [],
      jacketSizes: [],
      selectedJSize: 0,
      selectedPSize: 0,
      selectedBrand: 0
    }
  }
  componentDidMount(){
    this.fetchData();
  }

  fetchData(){
    const queries = [
      { searchQuery: 'brands', stateProp: 'brands'},
      { searchQuery: 'pants-sizes', stateProp: 'pantsSizes'},
      { searchQuery: 'jacket-sizes', stateProp: 'jacketSizes'}
    ]
    queries.map((query) => {
      let params = [];
      if(this.state.selectedJSize){
        params.push('jsize='+this.state.selectedJSize)
      }
      if(this.state.selectedPSize){
        params.push('psize='+this.state.selectedPSize)
      }
      if(this.state.selectedBrand){
        params.push('brand='+this.state.selectedBrand)
      }
      Client.search(query.searchQuery, [], results => {
        this.setState({ [query.stateProp]: results });
        return;
      });
      return { success: true};
    })
  }
  jSizeChange(event){
    this.setState({selectedJSize: event.target.value});
    this.props.onQuery({selectedJSize: event.target.value})
    this.fetchData();
  }
  pSizeChange(event){
    this.setState({selectedPSize: event.target.value})
    this.props.onQuery({selectedPSize: event.target.value});
    this.fetchData();
  }
  brandChange(event){
    this.setState({selectedBrand: event.target.value})
    this.props.onQuery({selectedBrand: event.target.value}); 
    this.fetchData();
  }  
  render(){
    return (
      <div className="container">
        <div className="row form-group">
          <div className="col-sm-4">
            <select name="jacketSize" className="form-control" value={this.state.selectedJSize} onChange={this.jSizeChange.bind(this)}>
              <option value="0"> - Jacket Size - </option>
              {this.state.jacketSizes.map((jacketSize, i) => {
                return <SelectOption value={jacketSize.sizeid} key={i} text={jacketSize.name} />
              })}
            </select>
          </div>
          <div className="col-sm-4">
            <select name="pantsSize" className="form-control" value={this.state.selectedPSize} onChange={this.pSizeChange.bind(this)}>
              <option value="0"> - Pants Size - </option>
              {this.state.pantsSizes.map((pantsSize, i) => {
                return <SelectOption value={pantsSize.sizeid} key={i} text={pantsSize.name} />
              })}
            </select>
          </div>
          <div className="col-sm-4">
            <select name="brand" className="form-control" value={this.state.selectedBrand} onChange={this.brandChange.bind(this)}>
              <option value="0"> - Brand - </option>
              {this.state.brands.map((brand, i) => {

                return <SelectOption value={brand.brandid} key={i} text={brand.name} />
              })}
            </select>
          </div>
        </div>
      </div>
    )
  }
}
export default SearchBar