import React, { Component } from 'react';

class Product extends Component{
  render(){
    return(
      <div className="product-box col-md-6">
        <a target="_blank" 
          href={this.props.detailPageURL}>
          <img src={this.props.mediumImage} width="120" height="160" role="presentation"/>
        </a>
        <div className="product-title">
          <h3>{this.props.title}</h3>
        </div>
        <p className="product-price">
          {this.props.lowestNewPriceFormatted}
          <br/>
          <a target="_blank" 
            href={this.props.moreOffersUrl} >
            More offers 
          </a>
        </p>
        <div>
          <span className="a-button a-button-primary">
            <a target="_blank" href={this.props.detailPageURL}>
              <span className="a-button-inner">
                <img src="assets/images/Amazon-Favicon-64x64.png" role="presentation" className="a-icon a-icon-shop-now" />
                <input className="a-button-input" type="submit" value="Add to cart" />
                <span className="a-button-text">Shop Now</span>
              </span>
            </a>
          </span>
        </div>
      </div>
    )
  }
}

export default Product;