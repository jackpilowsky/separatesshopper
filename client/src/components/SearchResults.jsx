import React, { Component } from 'react';
import ProductPair from "./ProductPair.jsx";
import Client from "./../Client.js";

class SearchResults extends Component{
  constructor(props){
    super(props);
    this.state = {
      matches: []
    };
  }  
  componentWillReceiveProps(nextProps){
    let params = [];
    if(nextProps.selectedJSize){
      params.push('jsize='+nextProps.selectedJSize)
    }
    if(nextProps.selectedPSize){
      params.push('psize='+nextProps.selectedPSize)
    }
    if(nextProps.selectedBrand){
      params.push('brand='+nextProps.selectedBrand)
    }
    console.log(params)
    Client.search('matches', params, results => {
        this.setState({ matches: results });
        return;
    });
  }  
  render(){
    return (
      <div className="container">
        {this.state.matches.map((match, i) => {
          return <ProductPair 
                    ptitle={match.ptitle}
                    pcolor={match.pcolor}
                    pdetailPageURL={match.pdetailPageURL}
                    pmediumImage={match.pmediumImage}
                    plargeImage={match.plargeImage}
                    pLowestNewPrice={match.pLowestNewPrice}
                    pLowestNewPriceFormatted={match.pLowestNewPriceFormatted}
                    pmoreOffersUrl={match.pmoreOffersUrl}
                    jtitle={match.jtitle}
                    jcolor={match.jcolor}
                    jdetailPageURL={match.jdetailPageURL}
                    jmediumImage={match.jmediumImage}
                    jlargeImage={match.jlargeImage}
                    jLowestNewPrice={match.jLowestNewPrice}
                    jLowestNewPriceFormatted={match.jLowestNewPriceFormatted}
                    jmoreOffersUrl={match.jmoreOffersUrl}
                    key={i}  />
        })}
      </div>
    )
  }
}
export default SearchResults