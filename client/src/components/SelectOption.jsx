import React, { Component } from 'react';

class SelectOption extends Component{
	render(){
		return (
			<option value={this.props.value} key={this.props.id}>{this.props.text}</option>
		)
	}	
}
export default SelectOption