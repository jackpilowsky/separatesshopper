import React, { Component } from 'react';
import Product from "./Product.jsx";

class ProductPair extends Component{
  render(){
    console.log(this.props)
    return (
      <div className="row">
        <Product title={this.props.ptitle}
          color={this.props.pcolor}
          detailPageURL={this.props.pDetailPageURL}
          mediumImage={this.props.pmediumImage}
          largeImage={this.props.plargeImage}
          LowestNewPrice={this.props.pLowestNewPrice}
          LowestNewPriceFormatted={this.props.pLowestNewPriceFormatted}
          moreOffersUrl={this.props.pmoreOffersUrl} />
        <Product title={this.props.jtitle}
          color={this.props.jcolor}
          detailPageURL={this.props.jDetailPageURL}
          mediumImage={this.props.jmediumImage}
          largeImage={this.props.jlargeImage}
          LowestNewPrice={this.props.jLowestNewPrice}
          LowestNewPriceFormatted={this.props.jLowestNewPriceFormatted}
          moreOffersUrl={this.props.jmoreOffersUrl} />
      </div>
    )
  }
}
export default ProductPair