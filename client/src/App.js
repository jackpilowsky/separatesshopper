import React, { Component } from "react";
import HeaderRow from "./components/HeaderRow.jsx";
import SearchBar from "./components/SearchBar.jsx";
import SearchResults from "./components/SearchResults.jsx";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      selectedJSize : 0,
      selectedPSize : 0,
      selectedBrand : 0
    }
  }  
  handleQuery(query){
    for(let key in query){
      if(!query.hasOwnProperty(key)){
        continue
      }
      this.setState({ [key]: query[key] });
    }
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <HeaderRow />
          <SearchBar onQuery={this.handleQuery.bind(this)} />
          <SearchResults selectedJSize={this.state.selectedJSize} selectedPSize={this.state.selectedPSize} selectedBrand={this.state.selectedBrand} />
        </div>
      </div>
    );
  }
}

export default App;
